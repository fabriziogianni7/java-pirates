package com.pirate.java.esercitazioni.quarantena.cicli;
/*  1. calcola 1+2+…+N (demo)
	2. calcola 1*2*…*N
	3. stampa i valori di un array di stringhe
	4. stampa 3 righe, ciascuna con 4 asterischi(demo)
	5. stampa 4 righe di asterischi di lunghezza 1, 2, 3, 4
	6. stampa asterischi nelle colonne pari e $ nelle colonne dispari
	7. stampa uno schema a scacchiera di asterischi
	8. Scrivere un metodoche, dato un intero N, stampi una matrice NxN il cui elemento (i, j) vale: –1 se i è un divisore di j (o viceversa); –0 altrimenti.
	9. Scrivere un metodo che, presi in ingresso un testo sotto forma di stringa e una parola w, trasformi il testo in parole(token) e conti le occorrenze di w nel testo
	10. Scrivere un metodo che legge una stringa da console e la stampa in verticale un carattere per linea 
	11. Scrivere un metodo che riceve tre stringhe e le stampa in verticale una accanto all’altra
	12. Scrivere un metodo che riceve una stringa e stampaa video il conteggio delle vocali in essa contenute 
	13. Scrivere un metodo che, dato un intero positivo n in ingresso, stampi i divisori propri di n (ovvero i divisori < n) 
	14. Scrivere un metodo che, dati in ingresso due interi a e b e un terzo intero N, stampi a e b e gli N numeri della sequenza in cui ogni numero è la somma dei due precedenti •Ad esempio, dati gli interi 2, 3 e 6, il metodo stampa:
		2, 3, 5, 8, 13, 21, 34, 55*/
public class EserciziCicli {
	
//	 1. calcola 1+2+…+N (demo)
	private static int calcolaSommaN(int n) {
		int somma = 0;
		for (int i=1; i<=n; i++){
			somma +=i; //somma = somma+1
		}
		return somma;
	}
//	 calcola 1*2*…*N
	private static int calcolaProdottoN(int n) {
		int prodotto = 1;
		for (int i=1; i<=n; i++){
			prodotto *=i; 
		}
		return prodotto;
	}
//	3. stampa i valori di un array di stringhe
	private static void stampaarrayStringhe() {
		String[] stringArray = {"questo", "è", "un","array","di","stringhe"};
		for(int i=0; i<stringArray.length; i++) {
			System.out.print(stringArray[i]+" ");
		}
		System.out.println();
	}
//	 stampa 3 righe, ciascuna con 4 asterischi(demo)
	private static void stampa3RigheCon4Asterischi() {
		for(int i=1; i<=3; i++) {
			for(int x =1; x<=4;x++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
//	5. stampa 4 righe di asterischi di lunghezza 1, 2, 3, 4
	private static void stampa4RigheLunghezza1234() {
		for(int i=1; i<=4; i++) {
			for(int x =1; x<=i;x++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
//	6. stampa asterischi nelle colonne pari e $ nelle colonne dispari
	private static void stampaAsterischiDollari() {
		for(int i=1; i<=3; i++) {
			for(int x =1; x<=5;x++) {
				if(x%2 ==0)
					System.out.print("*");
				else
					System.out.print("$");//le parentesi graffe si possono omettere se dopo l'if c'è solo una riga
			}
			System.out.println();
		}
	}
//	7. stampa uno schema a scacchiera di asterischi
	private static void stampaScacchiera() {
		for(int i=1; i<=3; i++) {
			for(int x =1; x<=5;x++) {
				if((i+x)%2 ==0)
					System.out.print("*");
				else
					System.out.print(" ");//le parentesi graffe si possono omettere se dopo l'if c'è solo una riga
			}
			System.out.println();
		}
	}
//	8. Scrivere un metodoche, dato un intero N, stampi una matrice NxN il cui elemento (i, j) vale: 
//	–1 se i è un divisore di j (o viceversa); 
//	–0 altrimenti.
	private static void matriceNxN() {
		int N = 10;
		for(int i = 1; i <= N; i++) {
			for(int j = 1; j <= N; j++) {
				if (i/j == 1 || j/i == 1)
					System.out.print(1);
				else
					System.out.print(0);
			}
			System.out.println();
		}
	}
//	9. Scrivere un metodo che, presi in ingresso un testo sotto forma di stringa e una parola w, 
//		trasformi il testo in parole(token) e conti le occorrenze di w nel testo
	private static int contaParola(String frase, String parola) {
		String[] tokens = frase.split(" ");
		int cont=0;
		for (int i = 0; i < tokens.length; i++) {
			if(tokens[i].equals(parola)) 
				cont++;
		}
		return cont;
	}
//	10. Scrivere un metodo che legge una stringa da console e la stampa in verticale un carattere per linea
	private static void stampaVert(String parola) {
		for(int i=0; i<=parola.length()-1; i++) {
			System.out.print(parola.charAt(i));
			System.out.println();
			
		}
	}
//	11. Scrivere un metodo che riceve tre stringhe e le stampa in verticale una accanto all’altra
	private static void stampaVertStringhe(String s,String s2,String s3) {
		int lungezzaSecondoParPerIlMathMax = Math.max(s2.length(),s3.length());
		int lunghezzaMax = Math.max(s.length(), lungezzaSecondoParPerIlMathMax);
		
		for(int i = 0; i<lunghezzaMax; i++) {
			if(i >= s.length())
				System.out.print(" ");
			else 
				System.out.print(s.charAt(i));
			if(i >= s2.length())
				System.out.print(" ");
			else 
				System.out.print(s2.charAt(i));
			if(i >= s3.length())
				System.out.print(" ");
			else 
				System.out.print(s3.charAt(i));
			System.out.println();
		}
	}
//	12. Scrivere un metodo che riceve una stringa e stampaa video il conteggio delle vocali in essa contenute 
	private static void contaVocali(String s) {
		int a=0;
		int e=0;
		int i=0;
		int o=0;
		int u=0;
		
		for(int x = 0; x < s.length(); x++) {
			String l = s.substring(x, x+1);
			switch (l) {
			//lo switch case non l'abbiamo ancora visto. è un altro modo per scrivere un blocco condizionale
			case "a" : a++;break;
			case "e" : e++;break;
			case "i" : i++;break;
			case "o" : o++;break;
			case "u" : u++;break;
			default: break;
			}
		}
		System.out.print("a: "+a+" e: "+e+" i: "+i+" o: "+o+" u: "+u);
	}
//	13. Scrivere un metodo che, dato un intero positivo n in ingresso, stampi i divisori propri di n (ovvero i divisori < n)
	private static void stampaDivisori(int n) {
		for(int i=1; i<=n; i++) {
			if(n%i ==0)
				System.out.println(i);
		}
	}
//	14. Scrivere un metodo che, dati in ingresso due interi a e b e un terzo intero N, stampi a e b e gli N numeri della sequenza in cui ogni numero è la somma dei due precedenti •Ad esempio, dati gli interi 2, 3 e 6, il metodo stampa:
//		2, 3, 5, 8, 13, 21, 34, 55
	private static void stampaNumeri(int a, int b,int N) {
		int somma = 0;
		for (int i=0; i<=N; i++) {
			somma = a+b;
			a=b;
			b = somma;
			System.out.println(somma);
		}
	}
	public static void main(String[] args) {
		System.out.println(calcolaSommaN(5));
		System.out.println();
		System.out.println(calcolaProdottoN(5));
		System.out.println();
		stampaarrayStringhe();
		System.out.println();
		stampa3RigheCon4Asterischi();
		System.out.println();
		stampa4RigheLunghezza1234();
		System.out.println();
		stampaAsterischiDollari();
		System.out.println();
		stampaScacchiera();
		System.out.println();
		 matriceNxN();
		System.out.println();
		contaParola("ciao ciao ciao ok ciao", "ciao");
		System.out.println();
		stampaVert("parola");
		System.out.println();
		stampaVertStringhe("ciao","sucaaaaaa","stai");
		System.out.println();
		 contaVocali("aeieioieueioeeiieieiieeieiuuaaaaeeuio");
		 System.out.println();
		 stampaDivisori(18);
		 System.out.println();
		 stampaNumeri(2,3,6);
		 System.out.println();
	}
}
