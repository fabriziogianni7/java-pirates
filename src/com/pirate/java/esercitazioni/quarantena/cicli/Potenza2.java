package com.pirate.java.esercitazioni.quarantena.cicli;
//calcolare la potenza di 2 fino a 2^N
public class Potenza2 {

	
	public static void main(String[] args) {
		int N = 5;
		int potenza = 1;
//		int i = 0;<--inizializzazione variabile
//		while(i<=N <-- condizione booleana) {
//			System.out.println(potenza);
//			potenza *= 2; // valore = valore*2;
//			i++;<--incremento
//		}
		for(int i = 0/*iniz variabile*/; i<=N /*condiz booleana*/;i++ /*incremento*/) {
			System.out.println(potenza);
			potenza*=2;
		}

	}

}
