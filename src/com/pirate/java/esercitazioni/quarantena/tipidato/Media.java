package com.pirate.java.esercitazioni.quarantena.tipidato;
/*
 * Inseriti 3 numeri calcolarne la media.
 * 
 * step:
 * 1 - prendere 3 numeri  dall'esterno
 * 2 - calcolare la media
 * 
 * */
public class Media {


	public static double mediaInt(int a, int b, int c) {
		int x = (a+b+c)/3;
		return x;
	}
	
	public static void main(String[] args) {
		System.out.print(mediaInt(1,2,10));

	}

}
