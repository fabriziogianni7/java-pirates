package com.pirate.java.esercitazioni.quarantena.tipidato;

/*
 * riconoscere se un numero è pari o dispari
 * */
public class PariDispari {
	
	private static void pariDispari(int a) {
		
		if(a%2 == 0) {
			System.out.print("pari");
		}
		else {
		System.out.print("dispari");
		}
	}

	public static void main(String[] args) {
	
		int a = 2345;
		pariDispari(a);
	}
}



