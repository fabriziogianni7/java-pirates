package com.pirate.java.esercitazioni.quarantena.tipidato;

/*
 * condizionale: blocco if / comparatori e segni
 * 
 * */


public class Condizionale {

	//stampare ok se un numero è maggiore di 5 oppure nope se è minore
	public static void bloccoIf() {
		int numero = 21;
		
		if((numero > 5 && numero < 10) ||  numero == 6) { //&& and-e || o - oppure
			System.out.print("ok ");
		}else {
			System.out.print("nessuna condizione verificata ");
		}
		
		
		
//		if ( numero < 3){
//			System.out.print(" nope ");
//			
//		}
//		if(numero == 21 ) {
//			System.out.print(" 21 vittoria grande baldoria! ");
//			
//		}
//		if(numero != 30) {// ! significa non 
//			System.out.print(" niente");
//		}
	}
	
	
	public static void main(String[] args) {
		bloccoIf();
	}

}
