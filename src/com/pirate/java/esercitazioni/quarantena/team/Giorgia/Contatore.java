package com.pirate.java.esercitazioni.quarantena.team.Giorgia;
//superbrava
public class Contatore {

	//campi della classe
	private int numeroClasse;
	
	//costruttore
	public Contatore(int numero) {
		this.numeroClasse = numero;
	}
	
	//metodo incrementa
	public void incrementa() {
		numeroClasse++;
	}
	
	//metodo decrementa
	public void decrementa() {
		numeroClasse--;
	}
	
	//metodo reset
	public void reset() {
	   
		numeroClasse = 0;
	}
	
	
	public int getNumeroClasse() {
		return numeroClasse;
	}

	//metodo inserisci input
	public int valoreInInput(int valoreInInput) {
		numeroClasse= valoreInInput;
		return numeroClasse;
	}
	
	
}
