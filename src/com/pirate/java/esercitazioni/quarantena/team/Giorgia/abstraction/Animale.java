package com.pirate.java.esercitazioni.quarantena.team.Giorgia.abstraction;
//•La classe possiede i metodi emettiVerso()e getNumeroDiZampe() 
//•Possiede inoltre il metodo getTaglia() che restituisce un valore scelto tra: piccola, media e grande. 
//•Progettare quindi le classi 
//Mammifero, Felino, Gatto(taglia piccola), Tigre(grande), 
//Cane, Chihuahua(piccola), Beagle (media),Terranova (grande), 
//Uccello, Corvo(media), Passero (piccola), Millepiedi (piccola) 
//•Personalizzare in modo appropriato la taglia, il numero di zampe e il verso degli animali



public abstract class Animale {
     protected String sound;
     protected int legs;
     protected String size;
	
	public Animale(String sound, int legs, String size){
		this.sound = sound;
		this.legs= legs;
		this.size= size;
	}
	
	protected abstract String makeSound();
	
	protected abstract int getNumberLegs();
	
	protected abstract String getSize();

	public String animalDatas() {
		return ("\nSound is: " + sound + "\n" + "number of legs is: " + legs + "\n" + "size is:" + size);
	}

}
