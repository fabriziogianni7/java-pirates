package com.pirate.java.esercitazioni.quarantena.team.Giorgia;

import java.util.Scanner;

public class Main {

	// CONTATORE
	// creazione oggetto
	public static void main(String[] args) {
		System.out.println("Esercizio n�1");
		Contatore contatore = new Contatore(2);
		contatore.incrementa();
		System.out.println(contatore.getNumeroClasse());
		contatore.decrementa();
		System.out.println(contatore.getNumeroClasse());

		contatore.reset();
		System.out.println(contatore.getNumeroClasse());

		Scanner scanner = new Scanner(System.in);
		System.out.println("Scrivi un valore:");

		int valoreInInput = scanner.nextInt();
		System.out.println(contatore.valoreInInput(valoreInInput));

		// Chiedere all utente che operazione si vuole fare (che metodo chiamare)
		// e
		// aumentare diminuire resettare il contatore
		// finch�
		// l'utente non dice di voler chiudere il programma
		System.out.println("Esercizio n�2");
		
		for (int valoreInput1 = 1; valoreInput1<=6; valoreInput1++) {
		
		Scanner scanner1 = new Scanner(System.in);
		System.out.println("Dato il valore inserito, scegliere un metodo per eseguire un'azione:");
		System.out.println("1-INCREMENTA");
		System.out.println("2-DECREMENTA");
		System.out.println("3-RESET");
		System.out.println("4-RITORNO AL VALORE INIZIALE");
		System.out.println("5-CAMBIA VALORE:");
		System.out.println("6-ESCI");

			valoreInput1 = scanner1.nextInt();
			if (valoreInput1 == 1) {

				contatore.incrementa();
				System.out.println("1-Metodo incrementa:" + " " + "-->" + contatore.getNumeroClasse());
			}

			else if (valoreInput1 == 2) {

				contatore.decrementa();
				System.out.println("2-Metodo decrementa:" + " " + "-->" + contatore.getNumeroClasse());
			} else if (valoreInput1 == 3) {

				contatore.reset();
				System.out.println("3-Metodo reset:" + " " + "-->" +contatore.getNumeroClasse());
			} else if (valoreInput1 == 4) {
				// ritorno al valore iniziale
				System.out.println(contatore.valoreInInput(valoreInInput));

			} else if (valoreInput1 == 5) {
				// imposto un nuovo valore
				System.out.println("Scegli il nuovo valore:");
				int valoreInIngresso = scanner.nextInt();
			}else {
				System.out.println("fine");
			}
		  }
		}
		
	
}
