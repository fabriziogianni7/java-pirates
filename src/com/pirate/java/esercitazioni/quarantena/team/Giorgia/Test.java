package com.pirate.java.esercitazioni.quarantena.team.Giorgia;
import java.util.*;
//La class test crea:
//1-due oggetti della classe Punto con cordinate (1,3,8) e (4,4,7)
//2-un oggetto della classe Segmento che rappresenti il segmento che unisce i due punti di cui sopra
//ADDIZIONALE: Calcola la lunghezza del segmento nello spazio 3D
//                                                    ______________________
//PS formula calcolo distanza tra due punti P(a,b) = √(x2-x1)^2 - (y2-y1)^2
public class Test {

	public static void main(String[] args) {
		
		Punto primoPunto = new Punto(1,3,8);
		Punto secondoPunto = new Punto(4,4,7);
       
	    Segmento segmento = new Segmento(primoPunto, secondoPunto);
	
	double distanza =   Math.sqrt((secondoPunto.getX()-primoPunto.getX())*(secondoPunto.getX()-primoPunto.getX()) + (primoPunto.getY()-secondoPunto.getY())*(primoPunto.getY()-secondoPunto.getY()));    
	    System.out.println(distanza);
	}

}
