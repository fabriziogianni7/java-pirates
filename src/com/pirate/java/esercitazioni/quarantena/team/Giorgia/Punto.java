package com.pirate.java.esercitazioni.quarantena.team.Giorgia;
//La claase punto: rappresentazione di un punto nello spazio tridimensionale

public class Punto {

	private double x;
	private double y;
	private double z;
	
	//valori nelle parentesi sono valori in ingresso del metodo
	public Punto(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
}
