package com.pirate.java.esercitazioni.quarantena.team.Giorgia;

public class Palindrome {
		    
	    public static boolean istPalindrom(char[] parola){
	        boolean palindroma = false;
	        if(parola.length%2 == 0){
	            for(int i = 0; i < parola.length/2-1; i++){
	                if(parola[i] != parola[parola.length-i-1]){
	                    return false;
	                }else{
	                    palindroma = true;
	                }
	            }
	        }else{
	            for(int i = 0; i < (parola.length-1)/2-1; i++){
	                if(parola[i] != parola[parola.length-i-1]){
	                    return false;
	                }else{
	                    palindroma = true;
	                }
	            }
	        }
	        return palindroma;
	    }
	    
	    public static void main(String[] args) {
	        String parola = "reliefpfpfeiller";
	        char[] parolaarray = parola.toCharArray(); 
	        System.out.println(istPalindrom(parolaarray));       
	    }

	}