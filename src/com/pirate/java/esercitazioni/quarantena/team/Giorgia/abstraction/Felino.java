package com.pirate.java.esercitazioni.quarantena.team.Giorgia.abstraction;

public class Felino extends Mammifero {

	public Felino(String sound, int legs, String size) {
		super(sound, 4, size);

	}
	
	@Override
	protected String makeSound() {
		return sound;
	}

	
	@Override
	protected String getSize() {
		return size;
	}

	
}