//Progettare una classe Stringa42
//�Costruita a partire da tre stringhe in input, che concateni le stringhe inframezzate da spazi
//Conservi solo i primi 42 caratteri della stringa risultante--> concatena stringhe
//La classe deve poter:
//1- restituire la stringa di lunghezza massima 42
//2- restituire l'iniziale di tale stringa
//ESEMPIO: La stringa magica deve restituire l'iniziale di tale stringa
//3-retituire un booleano che indichi se la stringa pari al numero magico 42

package com.pirate.java.esercitazioni.quarantena.team.Giorgia;

public class Stringa42 {

	private static String String1 = "ciao Alfred, oggi � una bellissima giornata";
	private static String String2 = "come stai?";
	private static String String3 = "io abbastanza bene, grazie";
	static String S = String1 + String2 + String3;

	public static String inframezza() {

		String S = String.join("-----", String1, String2, String3);
		return S;
	}

	public static String restituisci42Caratteri() {
		String Primi42Caratteri = inframezza();
		if (Primi42Caratteri.length()>42)
			Primi42Caratteri= Primi42Caratteri.substring(0, 42);
			return Primi42Caratteri;
		}

		public static char restituisciPrimoCarattere() {
		
			char restituisciPrimoCar;
			restituisciPrimoCar = restituisci42Caratteri().charAt(0);
			
			return restituisciPrimoCar;
		}
		
		
		
		
	public static void main(String[] args) {

		System.out.println(inframezza());
		 System.out.println(restituisci42Caratteri());
		 System.out.println(restituisciPrimoCarattere());
	}

}
