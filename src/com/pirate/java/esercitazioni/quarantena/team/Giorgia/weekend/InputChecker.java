package com.pirate.java.esercitazioni.quarantena.team.Giorgia.weekend;
import java.util.*;
/*
 *  InputChecker Testo: 
 *  Scrivere un programma che stampi la domanda �Vuoi continuare?� e che attenda dati dall�utente. 
 *  Se l�utente immette �S�, �S�, �Ok�, �Certo� o �Perch� no?�, stampare �OK�.
 *  Se l�utente scrive �N� o �No�, stampare �Fine�. Negli altri casi, stampare �Dato non corretto�.
 *  Non considerare differenze tra maiuscolo e minuscolo, quindi anche �s� e �s� sono validi. 
 
 Consigli: Per la prima volta, l�esercizio introduce l�espressione decisionale �if�,
    cio� definisce il comportamento di un programma, se e solo se sono rispettate determinate condizioni.
    Nel metodo String risposta(), � consigliabile concatenare tutte le condizioni richieste dall�esercizio
 * 
 */

public class InputChecker {
	
	public String risposta;
	
	public InputChecker(String risposta) {
		this.risposta = risposta;
	}

	public String Risposta() {
		
		 if (risposta.equalsIgnoreCase("S")||risposta.equalsIgnoreCase("Si")||  risposta.equalsIgnoreCase("Certo") ||
				 risposta.equalsIgnoreCase("OK")||    risposta.equalsIgnoreCase("Perch� no?"))     return "OK"; 
		
		else if(risposta.equalsIgnoreCase("N")|| risposta.equalsIgnoreCase("No"))     return "Fine";
		
		else     return "Dato non corretto!"; 
	}
	
	public static void main(String [] args) {
		
		
		 System.out.println("Vuoi continuare? Inserisci la tua risposta");
		 Scanner in = new Scanner(System.in);
		 String risp = in.nextLine();
		 InputChecker input = new InputChecker(risp);
		 System.out.println(input.Risposta());
	}
}
