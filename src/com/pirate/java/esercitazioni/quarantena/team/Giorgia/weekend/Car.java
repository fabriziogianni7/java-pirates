package com.pirate.java.esercitazioni.quarantena.team.Giorgia.weekend;
/*
 * Testo: Progettare e realizzare una classe Car(automobile) con le propriet� seguenti.
 * Un�automobile ha una determinata resa del carburante misurata in litri/chilometri:
 * e una certa quantit� di carburante nel serbatoio.
 * La resa � specificata dal costruttore e il livello iniziale del carburante � a zero.
 * 
 * Fornire questi metodi:
 * 
 * un metodo drive per simulare il percorso di un�automobile per una certa distanza,
 * riducendo il livello di carburante nel serbatoio;
 * 
 * un metodo getGas, per ispezionare il livello corrente del carburante;
 * 
 * un metodo addGas per far rifornimento. 
 Consigli: S�invita a porre particolare attenzione al metodo drive(double km), che calcola il livello di carburante dopo un certo percorso. 
 */

public class Car {

	private double resa;
	private double carburante;
	
	public Car(double resa) {
		this.resa = resa;
		carburante = 0;
	}
	
	public void drive(double km) {
		carburante = carburante - (km/resa);
	}
	public double getGas() {
		return carburante;
	}
	public double addGas(double ricaricaCarburante) {
		carburante = carburante + ricaricaCarburante;
		return carburante;
	}
	
	public static void main(String [] args) {
		
		Car toyota = new Car(20);
		toyota.addGas(20);
		toyota.drive(100);
		System.out.println("Il quantitativo di carburante rimasto dopo 100km �: " + toyota.getGas() + "L");
		
	}
}
