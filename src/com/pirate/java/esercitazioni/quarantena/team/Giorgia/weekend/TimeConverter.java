package com.pirate.java.esercitazioni.quarantena.team.Giorgia.weekend;

import java.util.*;
/*
 * Testo: Scrivere un programma che, dato un certo tempo in giorni, ore, minuti e secondi, 
 * restituisca il numero totale di secondi. 
   
   Consigli: Si prega di osservare che un giorno ha 86400 secondi, un�ora ha 3600 secondi e un minuto 60 secondi.
 * 
 */

public class TimeConverter {

	public static final int GIORNO_SEC = 86400;
	public static final int ORE_SEC = 3600;
	public static final int MINUTI_SEC = 60;
	private int s, m, h, d;

	public TimeConverter(int ss, int mm, int hh, int dd) {

		this.s = ss;
		this.m = mm;
		this.h = hh;
		this.d = dd;
	}

	public int converter() {

		int secondiTotali = d*GIORNO_SEC + h*ORE_SEC + m*MINUTI_SEC; 
		return secondiTotali;
	}

	public static void main(String[] args) {

		 Scanner in = new Scanner(System.in);
		 System.out.println("Inserisci il numero dei giorni ");
         int dd = in.nextInt();
         System.out.println("Inserisci il numero delle ore ");   
         int hh = in.nextInt();
         System.out.println("Inserisci il numero dei minuti ");   
         int mm = in.nextInt();
         System.out.println("Inserisci il numero dei secondi ");
         int ss = in.nextInt();
         
         TimeConverter tempo = new TimeConverter(ss, mm, hh, dd);
         System.out.println("i secondi totali sono: " + tempo.converter());
	}
}
