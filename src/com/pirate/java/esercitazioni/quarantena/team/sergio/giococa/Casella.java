package com.pirate.java.esercitazioni.quarantena.team.sergio.giococa;

public abstract class Casella {
	
	
	protected String segno;

	public abstract String stampaCasella();
	public abstract int avanzaCasella(Giocatore giocatore);
	
	protected String getSegno() {
		return segno;
	}
	
}
