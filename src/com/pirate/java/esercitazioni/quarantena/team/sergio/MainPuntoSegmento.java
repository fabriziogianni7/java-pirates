/*
 * Progettare una classe Punto per la rappresentazione di un punto nello spazio tridimensionale e
 * una classe Segmento per rappresentare un segmento nello spazio tridimensionale 
 * Scrivere una classe di test che crei: 
 * -due oggetti della classe Punto con coordinate (1, 3, 8) e (4, 4, 7) 
 * -un oggetto della classe Segmento che rappresenti il segmento che unisce i due punti di cui sopra
 *  ADDIZIONALE: CALCOLA LA LUNGHEZZA DEL SEGMENTO NELLO SPAZIO 3D
 * */

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class MainPuntoSegmento {

	public static void main(String[] args) {

		Punto p = new Punto(1, 3, 8);
		Punto q = new Punto(4, 4, 7);

		Segmento s = new Segmento(p, q);

		double lunghezza = Math.sqrt((Math.pow((q.getX() - p.getX()), 2)) + (Math.pow((q.getY() - p.getY()), 2))
				+ (Math.pow((q.getZ() - p.getZ()), 2)));
		System.out.println(lunghezza);

	}

}
