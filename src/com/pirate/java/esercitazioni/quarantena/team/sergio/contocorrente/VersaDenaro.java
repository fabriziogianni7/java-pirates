package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class VersaDenaro extends OperazioneBancaria {

	private double versamento;

	public VersaDenaro(double versamento) {

		this.versamento = versamento;
	}

	@Override
	protected double esegui(ContoBancario conto) {
		double saldo = conto.setSaldo(conto.getSaldo() + versamento);
		return saldo;
	}

}
