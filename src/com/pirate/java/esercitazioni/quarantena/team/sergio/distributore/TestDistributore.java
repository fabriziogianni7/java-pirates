package com.pirate.java.esercitazioni.quarantena.team.sergio.distributore;

public class TestDistributore {

	public static void main(String[] args) {
		Distributore dis = new Distributore(5);
		dis.inserisciImporto(2);
		System.out.println();
		dis.getProdotto(3);
		System.out.println();
		dis.getSaldo();
		System.out.println();
		dis.getResto();

	}

}
