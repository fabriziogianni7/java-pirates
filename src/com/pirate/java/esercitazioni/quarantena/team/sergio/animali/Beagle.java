package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Beagle extends Cane {

	public Beagle(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);
		// TODO Auto-generated constructor stub
	}
	@Override
	protected String emettiVerso() {
		verso = "BAU!";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe = 4;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia="media";
		return taglia;
	}

}
