package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Corvo extends Uccelli{

	public Corvo(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String emettiVerso() {
		verso="gracchia";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe=2;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia="Media";
		return taglia;
	}
	
}
