package com.pirate.java.esercitazioni.quarantena.team.sergio;


import java.util.Scanner;

//Scrivere un metodo che riceve una stringa e stampa a video il conteggio delle vocali in essa contenute 
public class Esercizio12 {

	public static int vocaliS(String s) {
		int vocali = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i' || s.charAt(i) == 'o'
					|| s.charAt(i) == 'u')
				vocali++;
		}

		return vocali;
	}

	public static void main(String[] args) {
		System.out.println("Inserire una stringa ");
		Scanner inputS = new Scanner(System.in);
		String s = inputS.nextLine();
		System.out.println(("Sono presenti ") + vocaliS(s) + (" vocali"));

	}

}
