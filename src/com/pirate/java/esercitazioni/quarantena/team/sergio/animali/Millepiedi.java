package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Millepiedi extends Animali {

	public Millepiedi(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String emettiVerso() {
		verso="???";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe=1000;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia="piccola";
		return taglia;
	}

}
