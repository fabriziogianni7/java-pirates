package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class SvuotaConto extends OperazioneBancaria {

	@Override
	protected double esegui(ContoBancario conto) {
		double saldo = conto.getSaldo();
		saldo = 0;
		return saldo;
	}

}
