package com.pirate.java.esercitazioni.quarantena.team.sergio;


//Scrivere un metodo che, dato un intero positivo n in ingresso, stampi i divisori propri di n (ovvero i divisori < n) 
public class Esercizio13 {

	public static void divisoriN(int n) {
		for (int i = 1; i < n; i++) {
			if (n % i == 0) {
				System.out.println(i);
			}

		}

	}

	public static void main(String[] args) {

		divisoriN(597);
	}

}
