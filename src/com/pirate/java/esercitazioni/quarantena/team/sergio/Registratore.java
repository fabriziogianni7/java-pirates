//Progettare una classe che costituisca un modello di registratore di cassa 
// * �La classe deve consentire a un cassiere di digitare i prezzi di articoli e la quantità di denaro 
// * pagata dal cliente, calcolando il resto dovuto 
// * 
// * �Quali metodi vi servono? 
// * �Registra il prezzo di vendita per un articolo -> PREZZO
// * �Registra la somma di denaro pagata -> PAGATO
// * �Calcola il resto dovuto al cliente -> PAGATO - PREZZO

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class Registratore {

	private double prezzo;
	private double soldi;

	public Registratore(double prezzo, double soldi) {

		this.prezzo = prezzo;
		this.soldi = soldi;
	}

	public double resto() {
		double resto = soldi - prezzo;
		return resto = soldi - prezzo;

	}

	public double prezzo() {
		return prezzo;

	}

	public double soldi() {
		return soldi;

	}

	public double controllo() {
		double mancante = 0;
		if (soldi < prezzo) {
			mancante = prezzo - soldi;
		}

		return mancante;

	}

}
