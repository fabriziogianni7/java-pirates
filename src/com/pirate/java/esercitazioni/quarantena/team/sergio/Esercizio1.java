package com.pirate.java.esercitazioni.quarantena.team.sergio;


public class Esercizio1 {
	
	public static int successione(int n) {
		int valore=1;
		for (int i=0; i<n; i++) {
			valore += i;
		}
		return valore;
	}

	public static void main(String[] args) {
		System.out.print(successione(90));
	}

}
