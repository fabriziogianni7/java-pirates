//Una stringa � palindroma se rimane uguale quando viene letta da sinistra verso destra o da destra verso sinistra
//�Scrivere un metodo che dica che una stringa � palindroma 
//�Scrivere anche una classe di collaudo che testi il metodo in diverse situazioni
//�Ad esempio, data in ingresso le stringhe �angelalavalalegna� o �itopinonavevanonipoti�, il metodo deve segnalare che la stringa � palindroma

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class Palindroma {

	private String s;

	public Palindroma(String s) {

		this.s = s;
	}

	public boolean reverse() {
		boolean b = false;
		String rev = "";
		for (int i = s.length() - 1; i >= 0; i--) {

			rev = rev + s.charAt(i);

		}

		b = s.equals(rev);

		return b;
	}

//	public String getS() {
//		return s;
//
//	}

//	public String getRev() {
//
//		return rev;
//	}
//		for (int i = 0; i < s.length(); i++) {
//			for (int j = 0; j < rev.length(); j++) {
//	if (s.charAt(i) == rev.charAt(j))		
//	}
}
