package com.pirate.java.esercitazioni.quarantena.team.sergio.giocodellocamodificato;

public class CasellaPunti extends Casella{

	public CasellaPunti(int posizione) {
		super(posizione);
	}

	@Override
	public String stampa() {
		String stampaPunti="$";
		if(!super.stampa().equals(""))
			return super.stampa()+stampaPunti;
		return stampaPunti;
	}

	@Override
	public Giocatore effetto(Giocatore giocatore) {
		giocatore.setPunti(giocatore.getPunti()+3);
		return giocatore;
	}

}
