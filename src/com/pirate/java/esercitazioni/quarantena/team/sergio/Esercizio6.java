package com.pirate.java.esercitazioni.quarantena.team.sergio;


//stampa asterischi nelle colonne pari e $ nelle colonne dispari

public class Esercizio6 {

	public static void asterischiDollaro(int n, int m) {

		for (int j = 0; j < n; j++) {
			for (int i = 0; i < m; i++) {
				if (i % 2 == 0) {
					System.out.print("$");
				} else
					System.out.print("*");

			}
			System.out.println();
		}

	}

	public static void main(String[] args) {

		asterischiDollaro(5, 8);

	}

}
