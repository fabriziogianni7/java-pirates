package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class MainRegistratore {

	public static void main(String[] args) {

		Registratore reg = new Registratore(20, 20);
		if (reg.controllo() <= 0) {
			System.out.println("Il cliente ha dato " + reg.soldi() + "�");
			System.out.println("Il prezzo � " + reg.prezzo() + "�");
			System.out.println("Il resto � di " + reg.resto() + "�");
		} else
			System.out.println("Mancano " + reg.controllo() + "�");
	}

}
