package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class Bonifico extends OperazioneBancaria {

	private ContoBancario contoFinal;
	private double importo;
	
	public Bonifico(ContoBancario contoFinal, double importo) {
		this.contoFinal=contoFinal;
		this.importo=importo;
		
	}
	
	@Override
	protected double esegui(ContoBancario conto) {
		conto.setSaldo(conto.getSaldo()-importo);
		double saldoFinal = conto.setSaldo(contoFinal.getSaldo()+importo);
		return saldoFinal;
	}

}
