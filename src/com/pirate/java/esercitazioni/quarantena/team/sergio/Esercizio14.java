package com.pirate.java.esercitazioni.quarantena.team.sergio;

//Scrivere un metodo che, dati in ingresso due interi a e b e un terzo intero N, stampi a e b e gli N numeri della sequenza in cui ogni numero � la somma dei due precedenti 
//Ad esempio, dati gli interi 2, 3 e 6, il metodo stampa:
//	Esercizio: somma dei due numeri precedenti
//	2, 3, 5, 8, 13, 21, 34, 55
public class Esercizio14 {
	
	public static void sommatoria(int a, int b, int n) {
		int c=0;
		System.out.println(a);
		System.out.println(b);
		for (int i=0; i<n; i++) {
			c=a+b;
			a=b;
			b=c;
			System.out.println(c);
		}
		
	}
	

	public static void main(String[] args) {
		sommatoria(2,3,6);
	}

}
