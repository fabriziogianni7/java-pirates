package com.pirate.java.esercitazioni.quarantena.team.sergio;


//Scrivere un metodo che, dato un intero N, 

//stampi una matrice NxN il cui elemento (i, j) vale: 1 se i � un divisore di j (o viceversa); 0 altrimenti.

public class Esercizio8 {

	public static void matrice(int n) {
		for (int i = 1; i < n; i++) {
			for (int j = 1; j < n; j++) {
//				if (i==0 || j==0) {
//					System.out.print("0");
//				}
				 if (i % j == 0 || j % i == 0) {
					System.out.print("1");
				} else
					System.out.print("0");
			}
			System.out.println();
		}

	}

	public static void main(String[] args) {
		matrice(8);

	}

}
