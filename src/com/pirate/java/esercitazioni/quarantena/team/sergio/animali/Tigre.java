package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Tigre extends Mammiferi{

	public Tigre(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String emettiVerso() {       	
		verso="ROAR!";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe = 4;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia = "Grande";
		return taglia;
	}
	
}
