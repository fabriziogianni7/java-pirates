package com.pirate.java.esercitazioni.quarantena.team.sergio.distributore;

public class Distributore {

	private int nProdotti;
	private Prodotto[] prodotti;
	private double saldo;

	public Distributore(int nProdotti) {
		this.nProdotti = nProdotti;
		this.prodotti = new Prodotto[nProdotti];
		carica();

	}

	public void carica() {
		for (int i = 0; i <= nProdotti - 1; i++) {
			double random = Math.random();
			if (random <= 0.3) {
				Caffe caffe = new Caffe();
				prodotti[i] = caffe;
			} else if (random >= 0.6) {
				Cappuccino cappuccino = new Cappuccino();
				prodotti[i] = cappuccino;
			} else {
				prodotti[i] = new Cioccolato();
			}

		}

		System.out.println("Prodotti inseriti:");
		for (int i = 0; i <= prodotti.length - 1; i++) {
			System.out.println("Prodotto n." + i + ": " + prodotti[i].getTipo());
			System.out.println("Prezzo: " + prodotti[i].getPrezzo());
			System.out.println();
		}

	}

	public double inserisciImporto(double importo) {
		saldo = saldo + importo;
		System.out.println("importo inserito" + importo);
		System.out.println("Saldo:" + saldo);
		return saldo;

	}

	public void getProdotto(int numeroProdotto) {
		Prodotto selezionato = prodotti[numeroProdotto];
		System.out.println("Il prodotto selezionato �: " + selezionato.getTipo());
		System.out.println("Prezzo: " + selezionato.getPrezzo() + "�");
		double saldonew = saldo - selezionato.getPrezzo();
		saldo = saldonew;
		System.out.println("Credito rimasto: " + saldo);

	}

	public double getSaldo() {
		return saldo;
	}
	
	public double getResto() {
		double resto=saldo;
		saldo =0;
		System.out.println("Il resto � " + resto);
		return resto;
	}
}