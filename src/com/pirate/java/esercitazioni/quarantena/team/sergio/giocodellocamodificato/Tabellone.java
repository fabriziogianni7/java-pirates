package com.pirate.java.esercitazioni.quarantena.team.sergio.giocodellocamodificato;

public class Tabellone {

	// in questo caso ho dato una costante (le costanti si scrivono in maiuscolo e
	// si dichiarano final -- andare a vedere cosa signigfica nel dettaglio
	private static final int N_CASELLE = 20;
	private Giocatore[] elencoGiocatori;
	private Casella[] listaCaselle = new Casella[getnCaselle()];

	public Tabellone(int nGiocatori) {
		this.elencoGiocatori = new Giocatore[nGiocatori];
		inizializzaTabellone();
		creaGiocatori();
		posizionaGiocatori();
	}

	public Giocatore[] getElencoGiocatori() {
		return elencoGiocatori;
	}

	public Casella[] getListaCaselle() {
		return listaCaselle;
	}

	public Casella[] inizializzaTabellone() {

		CasellaZero d = new CasellaZero(0);
		listaCaselle [0]= d;
		for (int i = 1; i < getnCaselle(); i++) {			
			int rand = (int) ((Math.random()) * 3 + 1);
			for (int e = 0; e <= elencoGiocatori.length - 1; e++) {
				if (rand == 1) {
					CasellaVuota c = new CasellaVuota(i);
					listaCaselle[i] = c;
				}
				if (rand == 2) {
					CasellaSpostaGiocatore c = new CasellaSpostaGiocatore(i);
					listaCaselle[i] = c;
				}
				if (rand == 3) {
					CasellaPunti c = new CasellaPunti(i);
					listaCaselle[i] = c;
				}
			}
		}
		return listaCaselle;
	}

	public void creaGiocatori() {
		for (int i = 0; i <= elencoGiocatori.length - 1; i++) {
			elencoGiocatori[i] = new Giocatore("G" + i);
		}
	}

	public void posizionaGiocatori() {
		for (Casella c : listaCaselle) {
			Giocatore[] giocatoriSuCasella = new Giocatore[elencoGiocatori.length];
			for (int g = 0; g <= elencoGiocatori.length - 1; g++) {
				Giocatore gioc = elencoGiocatori[g];
				if (gioc.getPosizione() > getnCaselle()) {
					gioc.setPosizione(gioc.getPosizione() - getnCaselle());
					gioc.setPunti(8);
				}
				if (gioc.getPosizione() == c.getPosizione()) {
					giocatoriSuCasella[g] = gioc;
					c.setGiocatoriSuCasella(giocatoriSuCasella);
				}
				if (gioc.getPosizione() != c.getPosizione()) {
					giocatoriSuCasella[g] = null;
					c.setGiocatoriSuCasella(giocatoriSuCasella);
				}
//				if (gioc.getPosizione() == getnCaselle()) {
//					System.out.println("Partita finita!");
//					break;
//				}
			}
		}
	}

	public int getnCaselle() {
		return N_CASELLE;
	}

}