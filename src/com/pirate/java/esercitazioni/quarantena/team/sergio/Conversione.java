////Progettare una classe per la conversione di base dei numeri interi 
//Ogni oggetto della classe viene costruito con un intero o con una stringa che contiene lintero
//La classe  in grado di convertire lintero nella base desiderata(restituito sotto forma di stringa)



/*l'esercizio va bene ma puo essere fatto meglio
proma a non usare i metodi di integer e crea un metodo tipo:
428 (base 10)->(base 6) = 1552
	public static double convertiDaB10(String num, int base)
che converte qualsiasi numero in qualsiasi base
*/
package com.pirate.java.esercitazioni.quarantena.team.sergio;

import java.lang.Integer;
import java.util.Scanner;

public class Conversione {

	private int partenza;
	private int base;

	public Conversione(int partenza, int base) {
		this.partenza = partenza;
		this.base = base;

	}

	public String baseDue() {
		String basedue = Integer.toBinaryString(partenza);
		return basedue;
	}

	public String baseHex() {
		String basehex = Integer.toHexString(partenza);
		return basehex;
	}

	public String baseOtto() {
		String baseotto = Integer.toOctalString(partenza);
		return baseotto;
	}

	public int getBase() {
		return base;
	}
	public String setPartenza() {
		Scanner inputPartenza = new Scanner(System.in);
		String partenza= inputPartenza.nextLine();
		return partenza;
		
	}
//	
//	public String getPartenza() {
//		
//		return partenza;
//	}
//	
//	public int setBase(int base) {
//		this.base=base;
//		return base;
//	}

}