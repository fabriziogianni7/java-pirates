package com.pirate.java.esercitazioni.quarantena.team.sergio;
//Progettare una classe MioArray i cui oggetti vengono costruiti con un array di interi (int[ ]),

//* La classe implementa i seguenti metodi: 

//	�contiene, che data in ingresso una posizione e un intero, restituisce true o false
//	se l�intero � contenuto in quella posizione nell�array 
//	
//	�somma2, che restituisce la somma dei primi due elementi dell�array. 
//	Se l�array � di lunghezza inferiore (info: la lunghezza dell�array a si ottiene con il campo speciale length,
//  quindi a.length),
//	restituisce il valore del primo elemento oppure 0 se l�array � vuoto 
//  
//	�scambia, che date in ingresso due posizioni intere, scambia i valori presenti nelle due posizioni dell�array 
//	(es. scambia(1, 3) trasforma l�array { 1, 2, 3, 4, 5 } in { 1, 4, 3, 2, 5 }) 
//	
//	�maxTripla: che restituisce il valore massimo tra il primo, l�ultimo e il valore in posizione intermedia dell�array 
//	(es. restituisce 3 se l�oggetto � costruito con { *1, 7, 5, *3, 0, 2, *2}, le posizione esaminate hanno l'asterisco) 
//	
//	�falloInDue: che restituisce un array di due interi, il primo � il primo elemento dell�array dell�oggetto,
//	il secondo � l�ultimo elemento dell�array dell�oggetto

public class MioArray {

	private int[] array;

	// costruttore

	public MioArray(int[] array) {
		this.array = array;

	}

	public int[] falloInDue() {
		int[] due = { array[0], array[array.length - 1] };
		return due;
	}

	public int maxTripla() {
		int primo = array[0];
		int ultimo = array[array.length - 1];
		int centro = array[(array.length - 1) / 2];
		int max = Math.max(primo, Math.max(ultimo, centro));

		return max;
	}

	public void scambia(int a, int b) {
		int c = array[b];
		array[b] = array[a];
		array[a] = c;

	}

	public boolean contiene(int p, int a) {

		boolean b = false;

		if (array[p] == a)

			b = true;

		return b;

	}

	public int somma2() {
		int somma = 0;
		if (array.length > 2)
			somma = array[0] + array[1];
		else
			somma = array[0];

		return somma;
	}

}
