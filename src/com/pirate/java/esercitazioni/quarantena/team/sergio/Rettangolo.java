package com.pirate.java.esercitazioni.quarantena.team.sergio;

////Progettare una classe Rettangolo
//i cui oggetti rappresentano un rettangolo e sono costruiti a partire dalle coordinate x, y e dalla lunghezza e altezza del rettangolo 
//�La classe implementa i seguenti metodi:
//�trasla che, dati in input due valori x e y, trasla le coordinate del rettangolo dei valori orizzontali e verticali corrispondenti 
//�toString che restituisce una stringa del tipo �(x1, y1)->(x2, y2)� 
//
//�toString che restituisce una stringa del tipo �(x1, y1)->(x2, y2)� con i punti degli angoli in alto a sinistra e in basso a destra del rettangolo 

//�Implementare una classe di test TestRettangolo
//che verifichi il funzionamento della classe Rettangolo sul rettangolo in posizione (0, 0) e di lunghezza 20 e altezza 10, traslandolo di 10 verso destra e 5 in basso

public class Rettangolo {

//dichiarazione variabili dell'oggetto

	private int x;
	private int y;
	private int lunghezza;
	private int altezza;

//costruttore

	public Rettangolo(int x, int y, int lunghezza, int altezza) {
		this.x = x;
		this.y = y;
		this.altezza = altezza;
		this.lunghezza = lunghezza;

	}
	
	
//metodi

	public void trasla(int x1, int y1) {
		x = x + x1;
		y = y + y1;

	}

	public String toString() {
		int x1 = x;
		int y1 = y + altezza;
		int x2 = x + lunghezza;
		int y2 = y;
		String punti = "(" + x1 + "," + y1 + ")->(" + x2 + "," + y2 + ")";
		return punti;
	}

}