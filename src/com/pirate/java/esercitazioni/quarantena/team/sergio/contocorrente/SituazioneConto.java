package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class SituazioneConto extends OperazioneBancaria {

	@Override
	protected double esegui(ContoBancario conto) {
		double saldo = conto.getSaldo();
		return saldo;
	}

}
