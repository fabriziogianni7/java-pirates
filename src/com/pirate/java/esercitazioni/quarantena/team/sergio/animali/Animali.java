//Progettare la classe Animale che rappresenti un generico animale 
//
//•La classe possiede i metodi emettiVerso()e getNumeroDiZampe() 
//•Possiede inoltre il metodo getTaglia() che restituisce un valore scelto tra: piccola, media e grande. 
//•Progettare quindi le classi Mammifero, Felino, Gatto(taglia piccola), Tigre(grande), Cane, Chihuahua(piccola), Beagle (media), Terranova (grande),
// Uccello, Corvo(media), Passero (piccola), Millepiedi (piccola)
//•Personalizzare in modo appropriato la taglia, il numero di zampe e il verso degli animali

package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public abstract class Animali {

	protected String verso;
	protected int zampe;
	protected String taglia;

	public Animali(String verso, int zampe, String taglia) {
		this.verso = verso;
		this.zampe = zampe;
		this.taglia = taglia;

	}

	protected abstract String emettiVerso();
	protected abstract int getNumeroDiZampe();
	protected abstract String getTaglia();
	
	
}
