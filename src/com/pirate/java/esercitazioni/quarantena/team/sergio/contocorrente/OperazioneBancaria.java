package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public abstract class OperazioneBancaria {

	protected abstract double esegui(ContoBancario conto);
	
	
}
