package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class MainConversione {

	public static void main(String[] args) {

		Conversione conv = new Conversione(12431, 2);

		if (conv.getBase() == 2)
			System.out.println("Il numero inserito in base due � " + conv.baseDue());
		else if (conv.getBase() == 8)
			System.out.println("Il numero inserito in base otto � " + conv.baseOtto());
		else if (conv.getBase() == 16)
			System.out.println("Il numero inserito in base sedici � " + conv.baseHex());

	}

}
