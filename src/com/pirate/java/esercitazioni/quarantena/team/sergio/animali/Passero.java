package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Passero extends Uccelli {

	public Passero(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected String emettiVerso() {
		verso="cinghuetta";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe=2;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia="Piccola";
		return taglia;
	}

}
