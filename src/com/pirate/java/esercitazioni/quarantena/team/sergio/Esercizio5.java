package com.pirate.java.esercitazioni.quarantena.team.sergio;


//stampa 4 righe di asterischi di lunghezza 1, 2, 3, 4

public class Esercizio5 {

	public static void main(String[] args) {
		char asterisco = '*';
		String s = "*";
		for (int i = 0; i < 4; i++) {
			System.out.println(s);
			s = s + asterisco;
		}

	}

}
