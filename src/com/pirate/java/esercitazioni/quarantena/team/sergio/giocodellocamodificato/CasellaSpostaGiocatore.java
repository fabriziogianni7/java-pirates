package com.pirate.java.esercitazioni.quarantena.team.sergio.giocodellocamodificato;

public class CasellaSpostaGiocatore extends Casella{

	public CasellaSpostaGiocatore(int posizione) {
		super(posizione);
	}

	@Override
	public String stampa() {
		String stampaSposta="^";
		if(!super.stampa().equals(""))
			return super.stampa()+stampaSposta;
		return stampaSposta;
	}

	@Override
	public Giocatore effetto(Giocatore giocatore) {
		giocatore.setPosizione(giocatore.getPosizione()+2);
		return giocatore;
	}

}
