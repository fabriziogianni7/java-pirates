package com.pirate.java.esercitazioni.quarantena.team.sergio.giococa;

public class CasellaVuota extends Casella {

	
	
	@Override
	public String stampaCasella() {
		segno = "O";
		return segno;
		
	}

	@Override
	public int avanzaCasella(Giocatore giocatore) {
		int posizione;
		posizione = giocatore.getPosizione()+giocatore.tiraDadi();
		giocatore.setPosizione(posizione);
		System.out.println(posizione);
		return posizione;
		
		
	}
	
	public String getCasella() {
		return segno;
	}

	

}
