package com.pirate.java.esercitazioni.quarantena.team.sergio;


import java.util.Arrays;
import java.util.StringTokenizer;
//Scrivere un metodo che, presi in ingresso un testo sotto forma di stringa e una parola w,
//trasformi il testo in parole(token) e conti le occorrenze di w nel testo

public class Esercizio9 {

	public static void testo(String s, String w) {
		int contatore = 0;

		StringTokenizer token = new StringTokenizer(s);
		// System.out.println(token.countTokens());
		while (token.hasMoreTokens()) {
			System.out.println(token.nextToken());
		}

		String[] arr = s.split(" ");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
			if (arr[i].equals(w)) {
				contatore++;
			}

		}
		System.out.println(contatore);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		testo("ciao Prova Due", "ciao");

	}

}
