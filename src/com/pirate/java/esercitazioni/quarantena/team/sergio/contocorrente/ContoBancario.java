package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class ContoBancario {

	protected String iban;
	protected double saldo;
	
	public ContoBancario(String iban, double saldo) {
		this.iban=iban;
		this.saldo=saldo;
	
	}
	
	public double getSaldo() {
		return saldo;
			}
	
	public double setSaldo(double saldo) {
		this.saldo=saldo;
		return saldo;
		
	}
}
