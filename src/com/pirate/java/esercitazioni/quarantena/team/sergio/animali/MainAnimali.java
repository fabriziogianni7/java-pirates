package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class MainAnimali {

	public static void main(String[] args) {
		Gatto g = new Gatto(null, 0, null);
		System.out.println(g.emettiVerso());
		System.out.println(g.getNumeroDiZampe());
		System.out.println(g.getTaglia());
		
		Passero p = new Passero(null, 0, null);
		System.out.println(p.emettiVerso());
	}

}
