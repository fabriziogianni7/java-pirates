package com.pirate.java.esercitazioni.quarantena.team.sergio.giocodellocamodificato;

import java.util.Scanner;

public class GiocoDellOca {
	public static void main(String[] args) {
		System.out.print("Benvenuto su GiocoDellOcaDiFabrizio, clicca un tasto qualunque per iniziare: ");
		Scanner sc = new Scanner(System.in);
		String avvia = sc.nextLine();
		if (!avvia.equalsIgnoreCase("")) {
			Tabellone t = new Tabellone(2);
			stampaTabellone(t);
			giocaTurno(t);
		}
	}

	private static void giocaTurno(Tabellone t) {
		Giocatore[] giocatori = t.getElencoGiocatori();
		{
			int j=0;
			do {
				for (Giocatore g : giocatori) {
					Scanner sc = new Scanner(System.in);
					System.out
							.print(g.getNome() + ", clicca 'g' ed effettua la tua mossa, altrimenti 'e' per uscire: ");
					String decisione = sc.nextLine();

					// mossa del giocatore
					if (decisione.equalsIgnoreCase("g")) {
						int dado = g.tiraDado();
						System.out.println(dado);
						g.setPosizione(g.getPosizione() + dado);
						j = g.getPosizione();
						if (g.getPosizione() == t.getnCaselle()) {
							break;
						}

						// riposizionamento
						Casella[] caselle = t.getListaCaselle();
						for (Casella c : caselle) {
							// effetto
							if (g.getPosizione() == c.getPosizione())
								c.effetto(g);
							t.posizionaGiocatori();
						}
					}
					if (decisione.equalsIgnoreCase("e")) {
						System.out.println("Si � scelto di terminare il gioco. Addio!");
						stampaTabellone(t);
						stampaClassifica(t);
						break;

					}
					stampaTabellone(t);
					stampaClassifica(t);

					if (g.getPosizione() == t.getnCaselle()) {
						break;
					}

				}

			} while (j!=t.getnCaselle());
			System.out.println("Fine Partita!");
		}
	}

	private static void stampaTabellone(Tabellone t) {
		for (int i = 0; i <= t.getListaCaselle().length - 1; i++) {
			System.out.print(t.getListaCaselle()[i].stampa());
		}
		System.out.println();
		System.out.println();
	}

	private static void stampaClassifica(Tabellone t) {
		for (int i = 0; i <= t.getElencoGiocatori().length - 1; i++) {
			Giocatore g = t.getElencoGiocatori()[i];
			System.out.println(g.getNome() + ": posizione: " + g.getPosizione() + "; punti: " + g.getPunti());
		}
		System.out.println();
		System.out.println();
	}

}
