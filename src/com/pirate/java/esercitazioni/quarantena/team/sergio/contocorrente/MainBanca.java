package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class MainBanca {

	public static void main(String[] args) {
		
		ContoBancario contoUno = new ContoBancario("IT213861298371267392", 1000);
		SituazioneConto situazioneConto = new SituazioneConto();
		PrelevaDenaro prelievo = new PrelevaDenaro(100);
		SvuotaConto svuota = new SvuotaConto();
		VersaDenaro versamento = new VersaDenaro(2000);
		ContoBancario contoDue = new ContoBancario("IT21431247129481274", 500);
		Bonifico bonifico = new Bonifico(contoDue, 200);
		
		System.out.println("Saldo iniziale: "+ situazioneConto.esegui(contoUno));
		System.out.println();
		System.out.println("Saldo dopo prelievo: "+ prelievo.esegui(contoUno));
		System.out.println();
		System.out.println("Saldo dopo versamento: "+ versamento.esegui(contoUno));
		System.out.println();
		bonifico.esegui(contoUno);
		System.out.println("Saldo conto di partenza dopo bonifico "+ situazioneConto.esegui(contoUno));
		System.out.println();
		System.out.println("Saldo conto destinatario dopo bonifico "+ situazioneConto.esegui(contoDue));
		System.out.println();
		System.out.println("Svuota conto "+ svuota.esegui(contoUno));
		

	}

}
