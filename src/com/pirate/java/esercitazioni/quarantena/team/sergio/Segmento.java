/*
 * Progettare una classe Punto per la rappresentazione di un punto nello spazio tridimensionale e
 * una classe Segmento per rappresentare un segmento nello spazio tridimensionale 
 * Scrivere una classe di test che crei: 
 * -due oggetti della classe Punto con coordinate (1, 3, 8) e (4, 4, 7) 
 * -un oggetto della classe Segmento che rappresenti il segmento che unisce i due punti di cui sopra
 *  ADDIZIONALE: CALCOLA LA LUNGHEZZA DEL SEGMENTO NELLO SPAZIO 3D
 * */

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class Segmento {

	private Punto a;
	private Punto b;
	
	public Segmento(Punto a, Punto b) {
		this.a=a;
		this.b=b;
		
	}
	
	
	
	
}
