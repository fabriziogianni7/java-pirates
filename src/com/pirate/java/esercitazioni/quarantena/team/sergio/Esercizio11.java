package com.pirate.java.esercitazioni.quarantena.team.sergio;


import java.util.Scanner;

//Scrivere un metodo che riceve tre stringhe e le stampa in verticale una accanto all�altra
public class Esercizio11 {

	public static void treStringhe(String s, String t, String u) {
		int max = Math.max(s.length(), Math.max(t.length(), u.length()));

//		for (int i = 0; i < max; i++) {
//			if (i >= s.length()) 
//				System.out.print(" ");
//			 else
//				System.out.print(s.charAt(i)+" ");
//			if (i >= t.length()) 
//				System.out.print(" ");
//			 else
//				System.out.print(t.charAt(i)+" ");
//			if (i >= u.length()) 
//				System.out.print(" ");
//			 else
//				System.out.print(u.charAt(i)+" ");
//			
//			System.out.println();
//		}
		for (int i=0; i<max; i++) {
		if (i >= s.length() || i>= t.length() ||i>= u.length()) {
			System.out.print(" ");
		} else {
			System.out.print(s.charAt(i));
			System.out.print(t.charAt(i));
			System.out.print(u.charAt(i));
			}System.out.println();
	}
		
	}

	public static void main(String[] args) {

		System.out.println("Inserire tre stringhe ");
		Scanner inputS = new Scanner(System.in);
		String s = inputS.nextLine();
		String t = inputS.nextLine();
		String u = inputS.nextLine();
		treStringhe(s, t, u);

	}

}
