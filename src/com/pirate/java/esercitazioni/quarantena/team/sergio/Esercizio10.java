package com.pirate.java.esercitazioni.quarantena.team.sergio;


import java.util.Scanner;

//Scrivere un metodo che legge una stringa da console e la stampa in verticale un carattere per linea 
public class Esercizio10 {

	public static void verticale(String s) {
		String[] arr = s.split("");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}

	}

	public static void main(String[] args) {
		System.out.println("Inserire una stringa: ");
		Scanner inputS = new Scanner(System.in);
		String s = inputS.nextLine();
		verticale(s);

	}

}
