package com.pirate.java.esercitazioni.quarantena.team.sergio.animali;

public class Gatto extends Mammiferi {

	public Gatto(String verso, int zampe, String taglia) {
		super(verso, zampe, taglia);

	}

	@Override
	protected String emettiVerso() {       	
		verso="MIAO!";
		return verso;
	}

	@Override
	protected int getNumeroDiZampe() {
		zampe = 4;
		return zampe;
	}

	@Override
	protected String getTaglia() {
		taglia = "Piccola";
		return taglia;
	}

}
