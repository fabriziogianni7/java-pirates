//Scrivere un metodo che, dato un intero N in ingresso, stampi una cornice NxN 
//�Ad esempio: dato l�intero 5 in ingresso il metodo stampa:
//
//*****
//*   *
//*   *
//*   *
//*****

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class Cornice {

	private int N;

	public Cornice(int N) {
		this.N = N;

	}

	public void matrice() {
		for (int i = 0; i < N; i++) {
			System.out.print("*");

		}
		System.out.println();

		for (int i = 0; i < N - 2; i++) {
			System.out.print("*");
			for (int j = 0; j < N - 2; j++) {
				System.out.print(" ");
			}
			System.out.print("*");
			System.out.println();

		}
		for (int i = 0; i < N; i++) {
			System.out.print("*");
			
		}

	}

}
