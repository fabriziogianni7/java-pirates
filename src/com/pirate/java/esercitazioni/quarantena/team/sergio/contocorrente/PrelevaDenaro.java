package com.pirate.java.esercitazioni.quarantena.team.sergio.contocorrente;

public class PrelevaDenaro extends OperazioneBancaria {

	private double prelievo;

	public PrelevaDenaro(double prelievo) {
		this.prelievo = prelievo;
	}

	@Override
	protected double esegui(ContoBancario conto) {
		double saldo = conto.setSaldo(conto.getSaldo() - prelievo);
		return saldo;
	}
	
}
