//Progettare una classe Stringa42,
//   costruita a partire da 3 stringhe in input, che concateni le stringhe inframezzate da spazi 
//   e conservi solo i primi 42 caratteri della stringa risultante --> concatenaStringhe
//    
//La classe deve poter: 
//    restituire la stringa di lunghezza massima 42
//    restituire l�iniziale di tale stringa
//     	
// Esempio: la Stringa magica

//   	restituire l'iniziale di tale stringa
//    	restituire un booleano che indichi se la stringa  è pari al numero magico 42 
//     
//     nell'esempio delle slide la risoluzione � differente ma questo svolgimento da 
//     pressochè lo stesso risultato

package com.pirate.java.esercitazioni.quarantena.team.sergio;

public class Stringa42 {

	private String a;
	private String b;
	private String c;

	public Stringa42(String a, String b, String c) {
		this.a = a;
		this.b = b;
		this.c = c;

	}

	
	
	
	public String concatena() {
		String spazio = " ";
		String stringaUnita = a.concat(spazio).concat(b).concat(spazio).concat(c);
		return stringaUnita;
	}

	public String primi42() {
		String caratteri = concatena();
		if (caratteri.length() > 42)
			caratteri=caratteri.substring(0, 42);
		return caratteri;
	}

	public char iniziale() {
		char c = primi42().charAt(0);		
		return c;
		
	}
	
	public boolean numeromagico() {
		boolean b=false;
		if (concatena().length()==42)
			b=true;
		return b;
	}
}
