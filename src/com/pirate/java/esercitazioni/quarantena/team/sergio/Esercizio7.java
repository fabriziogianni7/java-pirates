package com.pirate.java.esercitazioni.quarantena.team.sergio;


//stampa uno schema a scacchiera di asterischi

public class Esercizio7 {

	public static void scacchiera(int righe, int elementi) {

		for (int j = 0; j < righe; j++) {
			for (int i = 0; i < elementi; i++) {
				if (i % 2 == 0) {
					System.out.print("*");
				} else
					System.out.print(" ");

			}
			System.out.println();
		}

	}
	
	public static void main(String[] args) {
		scacchiera(4,8);

	}

}
