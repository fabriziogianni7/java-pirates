package com.pirate.java.esercitazioni.quarantena.team.Giacomo;

import java.util.Scanner;

public class Contatore {
private int numeroClasse;

   
	public Contatore(int numero) {
		this.numeroClasse = numero;
	}
	public int incrementa() {
	  return numeroClasse+1;
	}
		public int decrementa() {		
		return numeroClasse-1;
	}
		
	public int reimpostaNuovoValore(int valoredInIngresso) {
		numeroClasse = valoredInIngresso;
		return numeroClasse;
	}
	
	public int reset() {
	    numeroClasse = 1;
	    return numeroClasse;
	}
		
}



