package com.pirate.java.esercitazioni.quarantena.ereditarieta.esempio;

public abstract class Forma {
	
	//campi
	//protected � una keyword che rende qualcosa(campo/metodo) visibile 
	//alle sottoclassi e a tutte le classi del pacchetto
	protected static String colore;
	
	public Forma(String colore) {
		this.colore=colore;
	}
	
	protected String getColore() {
		return colore;
	}
	
	//il metodo astratto ha solamente la firma MA deve venire implementato dalle sottoclassi
	protected abstract double getPerimetro();
	
}
