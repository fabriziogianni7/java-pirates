package com.pirate.java.esercitazioni.quarantena.ereditarieta.esempio;

public class Quadrato extends Forma{

	private double lato;
	private String nome;
	
	//overloading
	public Quadrato(String colore, double lato) {
		//super,usato dentro un costruttore � una keyword che indica che ci si sta riferendo al costruttore della superclasse
		super(colore);
		this.lato = lato;
	}
	
	//overloading: creare pi� metodi con lo stesso nome ma con parametri differenti
	public Quadrato(String nome, String nome1) {
		//this, usato dentro un costruttore, serve a fare riferimento a un costruttore della stessa classe
		this(colore,3);
		this.nome = nome1;
	}
	

	//overriding : ridefinizione metodo ereditato da una superclasse -- aggiungiamo il corpo del metodo che prima non c'era
	@Override //annotation
	protected double getPerimetro() {
		double perimetro = lato*4;
		return perimetro;
	}

	

}
