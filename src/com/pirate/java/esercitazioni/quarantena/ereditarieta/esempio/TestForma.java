package com.pirate.java.esercitazioni.quarantena.ereditarieta.esempio;

public class TestForma {

	public static void main(String[] args) {
		Quadrato q = new Quadrato("rosso", 4); //costruttore
		Quadrato q1 = new Quadrato("blue", "Quadratino");
		System.out.println(q.getColore()); //metodo della superclasse
		System.out.println(q.getPerimetro()); //metodo della sottoclasse
	}

}
