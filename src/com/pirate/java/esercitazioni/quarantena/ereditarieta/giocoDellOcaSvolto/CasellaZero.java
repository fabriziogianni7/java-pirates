package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocoDellOcaSvolto;

public class CasellaZero extends Casella{

	public CasellaZero(int posizione) {
		super(posizione);
	}

	@Override
	public String stampa() {
		String stampaVuota="*";
		if(!super.stampa().equals(""))
			return super.stampa()+stampaVuota;
		return stampaVuota;
	}
	

	@Override
	public Giocatore effetto(Giocatore giocatore) {
		return giocatore;
	}

}
