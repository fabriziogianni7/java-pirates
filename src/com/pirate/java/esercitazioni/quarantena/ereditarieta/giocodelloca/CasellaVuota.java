package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca;

public class CasellaVuota extends Casella{

	
	@Override
	public String stampa() {
		//TODO:se il metodo della superclasse è diverso da "" aggiungo il simbolo della casella altrimenti ritorno solo il simbolo
		String stampa ="_ ";
		return stampa;
	}

	@Override
	public Giocatore effetto(Giocatore gioc) {
		return gioc;
	}
	

}
