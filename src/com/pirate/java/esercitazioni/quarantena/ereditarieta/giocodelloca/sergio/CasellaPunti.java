package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca.sergio;

public class CasellaPunti extends Casella {

	@Override
	public String stampa() {
		// TODO:se il metodo della superclasse è diverso da "" aggiungo il simbolo
		// della casella altrimenti ritorno solo il simbolo
		String stampaPunti = "$ ";
		if (!super.stampa().equals("")) {
			 return stampaPunti += super.stampa();
		}
		return stampaPunti;
	}

	@Override
	public Giocatore effetto(Giocatore gioc) {
		gioc.setPunti(gioc.getPunti() + 2);
		return gioc;
	}

}
