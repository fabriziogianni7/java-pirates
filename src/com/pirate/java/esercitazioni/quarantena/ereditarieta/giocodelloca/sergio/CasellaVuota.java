package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca.sergio;

public class CasellaVuota extends Casella{

	
	@Override
	public String stampa() {
		//TODO:se il metodo della superclasse è diverso da "" aggiungo il simbolo della casella altrimenti ritorno solo il simbolo
		String stampaVuota ="_ ";
		if (!super.stampa().equals("")) {
			 return stampaVuota += super.stampa();
		}
		
		return stampaVuota;
	}

	@Override
	public Giocatore effetto(Giocatore gioc) {
		return gioc;
	}
	

}
