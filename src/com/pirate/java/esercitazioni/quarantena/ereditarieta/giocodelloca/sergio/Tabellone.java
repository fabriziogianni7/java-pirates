package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca.sergio;
//–Il Tabellone come sequenza di caselle costruita a partire da un intero N e da un elenco di giocatori; 

//la classe dispone dell’operazione di posizionamento dei giocatori tenendo conto dell’effetto “gioco dell’oca�? 
//in cui, arrivati alla fine, si torna indietro  

public class Tabellone {
	
	//come scrivere le costa
	private static final int N_CASELLE = 20;
	private Casella[] listaCaselle = new Casella[N_CASELLE];
	private int nGiocatori;
	private Giocatore[] elencoGiocatori;
	
	public Casella[] getListaCaselle() {
		return listaCaselle;
	}

	public void setListaCaselle(Casella[] listaCaselle) {
		this.listaCaselle = listaCaselle;
	}

	public int getnGiocatori() {
		return nGiocatori;
	}

	public void setnGiocatori(int nGiocatori) {
		this.nGiocatori = nGiocatori;
	}

	public Giocatore[] getElencoGiocatori() {
		return elencoGiocatori;
	}

	public void setElencoGiocatori(Giocatore[] elencoGiocatori) {
		this.elencoGiocatori = elencoGiocatori;
	}

	public static int getnCaselle() {
		return N_CASELLE;
	}

	
	//il tabellone sarà pronto all'uso quando viene creato
	public Tabellone(int nGiocatori) {
		this.elencoGiocatori = new Giocatore[nGiocatori];
		inizializzaTabellone();
		creaGiocatori();
		posizionaGiocatore();
	}
	
	public Casella[] inizializzaTabellone() {
		//inserisci venti caselle nuove
		for(int i =0;i<=N_CASELLE-1;i++) {
			int rand = (int) (Math.random()*3+1);
			if(rand == 1) {
				listaCaselle[i] = new CasellaVuota();
			}
			if(rand == 2) {
				listaCaselle[i] = new CasellaSpostaGiocatore();
			}
			if(rand == 3) {
				listaCaselle[i] = new CasellaPunti();
			}
		
		}
		
		return listaCaselle;
	}
	
	public void creaGiocatori() {
		for(int i=0;i<=elencoGiocatori.length-1;i++) {
			elencoGiocatori[i] = new Giocatore("G"+i);
		}
	}
	
	//comparare la posizione di ciascun giocatore con ciascunsa casella
		//se è uguale dico alla casella quali e quanti giocatori contiene
	public void posizionaGiocatore() {
		//altro modo per scrivere i cicli for significa: per ogni casella dentro listaCaselle fai qualcosa
		for(Casella c : listaCaselle) { 
			//costruisco array contenente giocatori su ogni casella
			Giocatore[] giocatoriSuCasella = new Giocatore[elencoGiocatori.length];
			for(int g =0; g<=elencoGiocatori.length-1;g++) { 
				Giocatore gioc = elencoGiocatori[g];	
				if (gioc.getPosizione()>N_CASELLE) {
					gioc.setPosizione(gioc.getPosizione()-N_CASELLE);
				}			
				
				if(gioc.getPosizione() == c.getPosizione()) {
					giocatoriSuCasella[g] = gioc;
					c.setGiocatoriSuCasella(giocatoriSuCasella);
				}
				
				if(gioc.getPosizione() != c.getPosizione()) {
					giocatoriSuCasella[g] = null;
					c.setGiocatoriSuCasella(giocatoriSuCasella);
				}
				//TODO: fare si che le caselle contengano i giocatori del turno corrente e non quelli dei turni precedenti
				//TODO: fare si che il posizionamento riparte da 0 se il giocatore supera le 20 caselle
				
				
				
				
				
				
				
			}
			
		}
		
	}
	
	

}
