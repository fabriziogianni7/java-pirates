package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca;

public abstract class Casella {
	protected int posizione;
	protected Giocatore[] giocatoriSuCasella;

	public Giocatore[] getGiocatoriSuCasella() {
		return giocatoriSuCasella;
	}

	public void setGiocatoriSuCasella(Giocatore[] giocatoriSuCasella) {
		this.giocatoriSuCasella = giocatoriSuCasella;
	}

	public int getPosizione() {
		return posizione;
	}

	public void setPosizione(int posizione) {
		this.posizione = posizione;
	}
	
	public String stampa() {
		String stampa = "";
//		TODO:se giocatoriSUCasella sono diversi da nullmaggiungere restituire il nomemdel giocatore
//		TODO:nelle sottoclassi richimare questo metodo tramite super.stampa() e aggiungere il simbolo della casella
		return stampa;
	}
	public abstract Giocatore effetto(Giocatore gioc);

}
