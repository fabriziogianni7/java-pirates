package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca.sergio;

public class Giocatore {
	
	private String nome;
	private int posizione;

	private int punti;
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getPosizione() {
		return posizione;
	}
	
	public void setPosizione(int posizione) {
		this.posizione = posizione;
	}
	
	public int getPunti() {
		return punti;
	}
	
	public void setPunti(int punti) {
		this.punti = punti;
	}
	
	public Giocatore(String nome) {
		this.nome=nome;
		posizione=0;
		punti=0;
	}
	public int tiraDadi() {
		int risultato = (int) (Math.random()*6+1);
		return risultato;
	}
		

}
