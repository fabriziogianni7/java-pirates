package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca;

public class CasellaSpostaGiocatore extends Casella{

	@Override
	public String stampa() {
		//TODO:se il metodo della superclasse è diverso da "" aggiungo il simbolo della casella altrimenti ritorno solo il simbolo
		String stampa ="^ ";
		return stampa;
	}

	@Override
	public Giocatore effetto(Giocatore gioc) {
		gioc.setPosizione(gioc.getPosizione()+2);
		return gioc;
	}
	

}
