package com.pirate.java.esercitazioni.quarantena.ereditarieta.giocodelloca.sergio;

//import com.pirate.java.esercitazioni.quarantena.ereditarieta.giocoDellOcaSvolto.Giocatore;

public abstract class Casella {
	protected int posizione;
	protected Giocatore[] giocatoriSuCasella;

	public Giocatore[] getGiocatoriSuCasella() {
		return giocatoriSuCasella;
	}

	public void setGiocatoriSuCasella(Giocatore[] giocatoriSuCasella) {
		this.giocatoriSuCasella = giocatoriSuCasella;
	}

	public int getPosizione() {
		return posizione;
	}

	public void setPosizione(int posizione) {
		this.posizione = posizione;
	}
	
	public String stampa() {
		String stampa = "";
//		TODO:se giocatoriSUCasella sono diversi da null aggiungere restituire il nome del giocatore
//		TODO:nelle sottoclassi richimare questo metodo tramite super.stampa() e aggiungere il simbolo della casella
		if(giocatoriSuCasella != null) {
			for(Giocatore g : giocatoriSuCasella) {
				if(g!= null){
					stampa +=g.getNome();
				}
			}
		}
				
		return stampa;
		
//		if (giocatoriSuCasella != null) {
//			for (int i=0; i<giocatoriSuCasella.length-1; i++ ) {
//				if(g!=null) {
//					stampa+=g.getNome();
//				}
//			}
//		}
	}
	public abstract Giocatore effetto(Giocatore gioc);

}
