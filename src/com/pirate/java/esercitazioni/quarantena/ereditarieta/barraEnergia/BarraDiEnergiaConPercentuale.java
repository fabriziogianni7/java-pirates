package com.pirate.java.esercitazioni.quarantena.ereditarieta.barraEnergia;
/*Creare una classe BarraDiEnergia costruita con un intero k che ne determina la lunghezza massima.
 *  Inizialmente la barra � vuota. 
 *  La barra � dotata di un metodo per l�incremento unitario del suo livello di riempimento 
 *  e di un metodo rappresenta che ne fornisce la rappresentazione sotto forma di stringa 
 *  (es. se il livello � 3 su 10, la stringa sar� �OOO=======�. 
 *  
 *  
 *  Creare quindi una seconda classe BarraDiEnergiaConPercentuale che 
 *  fornisce una rappresentazione sotto forma di stringa come BarraDiEnergia 
 *  ma stampando in coda alla stringa la percentuale del livello di riempimento. 
 *  Per esempio, se il livello � 3 su 10, la stringa sar� �OOO======= 30%�.
*/

public class BarraDiEnergiaConPercentuale extends BarraDiEnergia{

	public BarraDiEnergiaConPercentuale(double k, double l) {
		super(k, l);
	}
	
	public String ritornaPercentuale() {
		double p = l/k *100;
		String percentuale = p+"%";
		return percentuale;
	}

}
