package com.pirate.java.esercitazioni.quarantena.ereditarieta.barraEnergia;

/*Creare una classe BarraDiEnergia costruita con un intero k che ne determina la lunghezza massima.
 *  Inizialmente la barra � vuota. 
 *  La barra � dotata di un metodo per l�incremento unitario del suo livello di riempimento 
 *  e di un metodo rappresenta che ne fornisce la rappresentazione sotto forma di stringa 
 *  (es. se il livello � 3 su 10, la stringa sar� �OOO=======�. 
 *  
 *  
 *  Creare quindi una seconda classe BarraDiEnergiaConPercentuale che 
 *  fornisce una rappresentazione sotto forma di stringa come BarraDiEnergia 
 *  ma stampando in coda alla stringa la percentuale del livello di riempimento. 
 *  Per esempio, se il livello � 3 su 10, la stringa sar� �OOO======= 30%�.
*/

public abstract class BarraDiEnergia {

	//campi
	protected double k; //lunghezza massima della barra di energia
	protected double l; //livello di energia
	
	//costruttore
	public BarraDiEnergia(double k, double l) {
		this.k = k;
		this.l = l;
	}
	
	protected double incrementaLivelloEnergia() {
		return l+=1; //l = l+1
	}
	
	
	
	/*
	 * come progettare un metodo:
	 * chi lo deve vedere? --> public/private...
	 * cosa deve ritornare? --> scelgo il valore
	 * ha bisogno di valori in ingresso? --> parametri nelle parentesi
	 * cosa deve fare il metodo? es. sommare? moltiplicare? modificare?
	 * su cosa posso basarmi nello stabilire come agisce il metodo --> variabili a disposizione
	 * che cosa conosco es conosco i cicli operatori somma, differenza, for, conosco i condizionali ecc
	 * sperimentare e debuggare
	 * 
	 * */
	
	protected String rappresenta() {
	
		String carica = "" ;
		for(int i = 1; i<=k; i++) {
			if(i<=l)
				carica+="0";
			else
				carica+="=";
		}
		
		return carica;
	}
	
	
}
