package com.pirate.java.esercitazioni.quarantena.ereditarieta.distributore;

public class Cioccolato extends Prodotto{

	public Cioccolato() {
		tipo = "cioccolato";
		prezzo = 0.50;
	}
}
