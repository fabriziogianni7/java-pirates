package com.pirate.java.esercitazioni.quarantena.ereditarieta.distributore;

public abstract class Prodotto {

	protected double prezzo;
	protected String tipo;
	
	protected String getNomeProdotto() {
		return tipo;
	}
	protected double getPrezzoProdotto() {
		return prezzo;
	}
}
