package com.pirate.java.esercitazioni.quarantena.ereditarieta.distributore;

public class Cappuccino extends Prodotto{

	public Cappuccino() {
		tipo = "cappuccino";
		prezzo = 0.60;
	}
}
