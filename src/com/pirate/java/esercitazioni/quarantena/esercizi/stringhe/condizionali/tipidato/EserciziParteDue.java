package com.pirate.java.esercitazioni.quarantena.esercizi.stringhe.condizionali.tipidato;

public class EserciziParteDue {
//Inserita una stringa, restituire il caratter centrale se la string è dispari
	//le due centrali se è pari
	
	public static void restituisciCaratteri(String s) {
		int indiceMeta = (s.length()-1)/2;
		if(s.length()%2 ==0) {
			System.out.println("la lunghezza è pari e i caratteri centrali sono: "+s.charAt(indiceMeta)+", "+s.charAt(indiceMeta+1));
		}else {
			System.out.println("la lunghezza è dispari e il carattere centrale è: "+s.charAt(indiceMeta));
		}
	}
	
	// controllare se pref è la parte iniziale di str
	public static boolean controllaPrefString(String str, String pref) {
		boolean isPref= false;
		String prefissoDaControllare = str.substring(0, pref.length());
		if(pref.equals(prefissoDaControllare))
			isPref =true;
		return isPref;
	}
	
	//scrivere una classe che simuli un lancio di dado
	private static int lanciaDado() {
		 int lancioDado = (int)((Math.random())*6+1);
		 return lancioDado;
	}
	
	public static void main(String[] args) {
		//1.
		restituisciCaratteri("ciaone");
		restituisciCaratteri("bello");
		//2.
		System.out.println("pref è un prefisso di str? "+controllaPrefString("ciaoneproprio", "ciaone"));
		System.out.println("pref è un prefisso di str? "+controllaPrefString("ciaproprio", "ciaone"));
		//3.
		System.out.println("il lancio del dado è: "+lanciaDado());
		System.out.println("il lancio del dado è: "+lanciaDado());
		System.out.println("il lancio del dado è: "+lanciaDado());
		System.out.println("il lancio del dado è: "+lanciaDado());
		System.out.println("il lancio del dado è: "+lanciaDado());
		
	}
}
