package com.pirate.java.esercitazioni.quarantena.esercizi.stringhe.condizionali.tipidato;

public class EserciziParteUno {

	//1. Inserita una Stringa Restituire lunghezza se maggiore di tre
	private static int restituisciLunghezza(String str) {
		int lunghezza = 0;
		if(str.length() > 3) {
			lunghezza = str.length();
		}
		
		return lunghezza;
	}
	
	//Calcolo delle soluzioni di equazione di secondo grado
	//ax2+bx+c=0
	private static void calcolaEquazione(double a, double b, double c) {
		//ax2+bx+c=0 con a!=0
		//formula:x1,2= (-b+-rad(b^2-4ac))/2a
		
		//delta: b^2-4ac+
		double delta= Math.pow(b, 2)-4*a*c;
		if(delta >0) {
			double x1 = (-b+Math.sqrt(delta))/2*a;
			double x2 = (-b-Math.sqrt(delta))/2*a;
			System.out.println("soluzione x1= "+x1+"; soluzione x2= "+x2);
		}
		else if(delta == 0) {
			double x = -b/2*a;
			System.out.println("l'equazione fornisce due soluzioni reali e coincidenti, x= "+x);
		}
		else if(delta < 0) {
			System.out.println("equazione non ha numeri reali");
		}
	} 
	
	//3. controllare se la stringa contiene due o tre lettere uguali nella parte centrali
	private static void controllaLettereCentraliStringa(String st) {
		int indiceMeta = (st.length()-1)/2;
//		char c = st.charAt(indiceMeta);
//		char c1 = st.charAt(indiceMeta+1);
		if(st.length()%2 ==0) {
			if(st.charAt(indiceMeta) == st.charAt(indiceMeta+1)) {
				System.out.println("la lunghezza della stringa è pari e lettere centrali sono uguali");
			}else {
				System.out.println("la lunghezza della stringa è pari e lettere centrali sono diverse");
			}
		}
		else if(st.charAt(indiceMeta-1) == st.charAt(indiceMeta) && st.charAt(indiceMeta) == st.charAt(indiceMeta+1)) {
			System.out.println("la lunghezza della stringa è dispari le lettere centrali sono uguali");
		}else {
			System.out.println("la lunghezza della stringa è dispari le lettere centrali sono diverse");
		}
	}
	
	
	public static void main(String[] args) {
		//1.
		System.out.println(restituisciLunghezza("ciao"));
		//2. 
		calcolaEquazione(1,5,4);
		calcolaEquazione(1,2,1);
		calcolaEquazione(8,4,6);
		//3
		controllaLettereCentraliStringa("ciiia");
		controllaLettereCentraliStringa("ciaone");
		controllaLettereCentraliStringa("aeeei");
		controllaLettereCentraliStringa("alles");
	}

}
