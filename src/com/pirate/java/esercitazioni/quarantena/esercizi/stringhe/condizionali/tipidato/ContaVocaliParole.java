package com.pirate.java.esercitazioni.quarantena.esercizi.stringhe.condizionali.tipidato;

public class ContaVocaliParole {

	/*4. Data da console una stringa composta da più parole e spazi, x esempio "tanto va la gatta al lardo che ci lascia lo zampino"
	 * -Contare se sono presenti delle vocali
	 * -Contare quante parole ci sono 
	 * 
	 * */
	
	//pushProva
	public static boolean contaVocali(String str) {
		if (str.contains("a") || 
				str.contains("e") ||
				str.contains("i") ||
				str.contains("o") ||
				str.contains("u")) {
			return true;
		}else
			return false;
	}
	public static int contaParole(String str) {
		String[] arrayParole = str.split(" ");
		int numeroParole =arrayParole.length;
		
		return numeroParole;
		
	}
	
	
	public static void main(String[] args) {
		System.out.println("ci sono vocali? "+contaVocali("tanto va la gatta al lardo che ci lascia lo zampino"));
		System.out.println(contaParole("tanto va la gatta al lardo che ci lascia lo zampino"));

	}

}
