package com.pirate.java.esercitazioni.quarantena.esercizi.stringhe.condizionali.tipidato;

public class ProvaGit {
/*
 * Istruzioni GIT:
 * quando si crea o modifica qualcosa:
 * tasto dx sul prgetto --> TEAM --> commit --> scrivere un messaggio che indichi cosa 
 * avete caricato PS se ci sono file unstaged (finestra accanto al commit message, selezionarli e cliccare sui + verdi in alto)
 * DOPO COMMIT--> click dx su progetto --> TEAM--> PULL
 * DOPO PULL --> DX PROGETTO --> TEAM --> PUSH
 * 
 * RECAP:
 * QUANDO SI VUOLE CONDIVIDERE DEL LAVORO:
 * 1) commit: salvare il lavoro online ma non condividerlo
 * 2) pull: aggiornare il proprio progetto in locale con il lavoro del resto del team
 * 3) push: condivide il tuo lavoro con il resto del team
 * 
 * QUANDO SI VUOLE AGGIORNARE IL PROPRIO PROGETTO LOCALE PBASTA SOLAMENTE FARE LA PULL
 * */
}
