package com.pirate.java.esercitazioni.quarantena.oggetti.rettangolo;

public class TestRettangolo {

	public static void main(String[] args) {
		
		Rettangolo rett = new Rettangolo(0, 0, 10, 20);
		Rettangolo rettSecondo = new Rettangolo(23, 11, 10, 20);
		System.out.println(rett.toString());
		System.out.println(rettSecondo.toString());
		rett.trasla(10, -5); 
		rettSecondo.trasla(10, -5); 
		System.out.println(rett.toString());
		System.out.println(rettSecondo.toString());

	}

}
