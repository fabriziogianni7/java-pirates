package com.pirate.java.esercitazioni.quarantena.oggetti.rettangolo;
/*
 * Progettare una classe Rettangolo i cui oggetti
	rappresentano un rettangolo e sono costruiti a partire
	dalle coordinate x, y e dalla lunghezza e altezza del
	rettangolo
	
	La classe implementa i seguenti metodi:
	â€“ trasla che, dati in input due valori x e y, trasla le coordinate del
	rettangolo dei valori orizzontali e verticali corrispondenti
	â€“ toString che restituisce una stringa del tipo â€œ(x1, y1)->(x2, y2)â€�
	con i punti degli angoli in alto a sinistra(x=0) e in basso a destra(y=0) del
	rettangolo
	
	 Implementare una classe di test TestRettangolo che
	verifichi il funzionamento della classe Rettangolo sul
	rettangolo in posizione (0, 0) e di lunghezza 20 e
	altezza 10, traslandolo di 10 verso destra e 5 in basso
	
 * L'oggetto rettangolo è composto da:
 * 
 * x,y,lunghezza,altezza --> campi
 * 
 * 
 * 
 * */
public class Rettangolo {

	//campi --> variabili, dati
	//costruttore --> metodo che crea gli oggetti
	//metodi --> fanno tante cose, eseguono comandi
	
	private int x;
	private int y;
	private int altezza;
	private int lunghezza;
	
	//valori nelle parentesi sono valori in ingresso del metodo
	public Rettangolo(int x, int y, int altezza, int lunghezza) {
		this.x = x;
		this.y = y;
		this.altezza = altezza;
		this.lunghezza = lunghezza;
	}
	//trasla che, dati in input due valori x e y, trasla le coordinate del rettangolo dei valori orizzontali e verticali corrispondenti
	public void trasla(int x1, int y2) {
		x = x+x1;
		y = y+y2;
	}
	
	//toString che restituisce una stringa del tipo “(x1, y1)->(x2, y2)” 
	//con i punti degli angoli in alto a sinistra e in basso a destra del rettangolo -- 
	//DOBBIAMO RESTITUIRE GLI ANGOLI IN ALTO A SX E IN BASSO A DX
	public String toString() {
		int x1 = x;
		int y1 = y+altezza;
		int x2 = x+lunghezza;
		int y2 = y;
		String punti = "("+x1+","+y1+")->("+x2+","+y2+")";
		return punti;
	}








//un metodo puo restituire anche un oggetto creato da noi stessi es Rettangolo
	public Rettangolo restRett() {
		Rettangolo rett = new Rettangolo(1, 2, 3, 4);
		return rett;
	}
	
}
