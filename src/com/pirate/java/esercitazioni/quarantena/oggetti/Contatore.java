package com.pirate.java.esercitazioni.quarantena.oggetti;

import java.util.Scanner;

/*
 * Implementare:
 * 
 * -un metodo decrementa(),
 * -un metodo reset() che riporti numeroClasse al valore iniziale
 * -un metodo reimpostaNuovoValore() che resetta numeroClasse con un valore in INPUT
 * 
 * */
public class Contatore {

	
	private int numeroClasse;
	
	public Contatore(int numero) {
		this.numeroClasse = numero;
	}
	
	public void incrementa() {
	   numeroClasse++;
	
	}
	
	public void decrementa() {
	   numeroClasse--;
	}
	
	public int getNumeroClasse() {
	   return numeroClasse;
	
	}
	
	public int reimpostaNuovoValore() {
		Scanner scanner = new Scanner(System.in);
		numeroClasse = scanner.nextInt();
		return numeroClasse;
		
		
	}
		
}
