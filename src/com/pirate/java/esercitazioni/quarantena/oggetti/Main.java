package com.pirate.java.esercitazioni.quarantena.oggetti;

public class Main {


	public static void main(String[] args) {
		OggettoEsempio nomeOggettoCreato = new OggettoEsempio(9,10);
		nomeOggettoCreato.setSecondi(100);
		System.out.println(nomeOggettoCreato.getSecondi());
		
		Contatore contatore = new Contatore(1);
		
		contatore.incrementa();
		contatore.decrementa();
	    contatore.reimpostaNuovoValore();
	 
		System.out.print(contatore.getNumeroClasse());
		}

}
