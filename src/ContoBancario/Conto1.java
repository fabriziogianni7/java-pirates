package ContoBancario;

public class Conto1 extends ContoBancario{

	public Conto1(double saldoIniziale, double codiceIban) {
		super(saldoIniziale, codiceIban);
		
	}
	
	@Override
	public double prelevaDenaro(double prelievo) {
		
		prelievo = saldoIniziale-prelievo;
		return prelievo;	
	}
	@Override
	public double versaDenaro(double versamento) {
			
			versamento = saldoIniziale + versamento;
			return versamento;	
		}
	

	@Override
	public double svuotaConto() {
		double saldoIniziale = 0;
		return saldoIniziale;
	}

	@Override
	public double getSaldo() {
		return saldoIniziale;
	}

}
