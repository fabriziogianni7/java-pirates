package ContoBancario;

//Progettare la classe ContoBancario che rappresenti un conto con informazioni 
//relative al denaro attualmente disponibile, il codice IBAN 
//�Modellare quindi una generica operazione bancaria Operazioneche disponga di un metodo esegui()
//�Modellare quindi i seguenti tipi di operazione:

//�PrelevaDenaro: preleva una specificata quantit� di denaro da un dato conto 
//�SvuotaConto: preleva tutto il denaro da un dato conto 
//�VersaDenaro: versa del denaro sul conto specificato 
//�SituazioneConto: stampa l�attuale saldo del conto 
//�Bonifico: preleva del denaro da un conto e lo versa su un altro

public abstract class ContoBancario {

	protected double saldoIniziale;
	protected double codiceIban;
	
	public ContoBancario (double saldoIniziale, double codiceIban) {
		
		this.saldoIniziale=saldoIniziale;
		this.codiceIban=codiceIban;
	}
	
	public abstract double prelevaDenaro(double prelievo) ;
	
	public abstract double versaDenaro(double versamento);
	
	public abstract double svuotaConto();
	
	public abstract double getSaldo() ;
	
}
